<?php

namespace wework\struct\checkin;

use wework\Utils;

class SpeOffDays
{
    /** @var int */
    public $timestamp = null;
    /** @var string */
    public $notes = null;
    /** @var CheckinTime[]|array */
    public $checkintime = null;

    public static function ParseFromArray($arr)
    {
        $info = new SpeOffDays();

        $info->timestamp = Utils::arrayGet($arr, "timestamp");
        $info->notes = Utils::arrayGet($arr, "notes");

        foreach ($arr["checkintime"] as $item) {
            $info->checkintime[] = CheckinTime::ParseFromArray($item);
        }

        return $info;
    }
}
