<?php

namespace wework\struct\checkin;

use wework\Utils;

class CheckinTime
{
    /** @var int */
    public $work_sec = null;
    /** @var int */
    public $off_work_sec = null;
    /** @var int */
    public $remind_work_sec = null;
    /** @var int */
    public $remind_off_work_sec = null;

    public static function ParseFromArray($arr)
    {
        $info = new CheckinTime();

        $info->work_sec = Utils::arrayGet($arr, "work_sec");
        $info->off_work_sec = Utils::arrayGet($arr, "off_work_sec");
        $info->remind_work_sec = Utils::arrayGet($arr, "remind_work_sec");
        $info->remind_off_work_sec = Utils::arrayGet($arr, "remind_off_work_sec");

        return $info;
    }
}
