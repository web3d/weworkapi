<?php

namespace wework\struct\checkin;

use wework\Utils;

class CheckinDataList
{
    /**
     * @var CheckinData[]|array
     */
    public $checkindata = null;

    static public function ParseFromArray($arr)
    {
        $info = new CheckinDataList();

        foreach ($arr["checkindata"] as $item) {
            $info->checkindata[] = CheckinData::ParseFromArray($item);
        }

        return $info;
    }
}
