<?php

namespace wework\struct\checkin;

use wework\Utils;

class WifiMacInfo
{
    /** @var string */
    public $wifiname = null;
    /** @var string */
    public $wifimac = null;

    public static function ParseFromArray($arr)
    {
        $info = new WifiMacInfo();

        $info->wifiname = Utils::arrayGet($arr, "wifiname");
        $info->wifimac = Utils::arrayGet($arr, "wifimac");

        return $info;
    }
}
