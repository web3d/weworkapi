<?php

namespace wework\struct\checkin;

use wework\Utils;

class CheckinGroup
{
    /** @var int,  1 固定  2自定义  3自由签到 */
    public $grouptype = null;
    /** @var int, */
    public $groupid = null;
    /** @var CheckinDate array */
    public $checkindate = null;
    /** @var SpeWorkDays[]|array */
    public $spe_workdays = null;
    /** @var SpeOffDays[]|array */
    public $spe_offdays = null;
    /** @var bool, default true */
    public $sync_holidays = null;
    /** @var string */
    public $groupname = null;
    /** @var bool */
    public $need_photo = null;
    /** @var WifiMacInfo[]|array */
    public $wifimac_infos = null;
    /** @var bool */
    public $note_can_use_local_pic = null;
    /** @var bool */
    public $allow_checkin_offworkday = null;
    /** @var bool */
    public $allow_apply_offworkday = null;
    /** @var LocInfo[]|array */
    public $loc_infos = null;

    public static function ParseFromArray($arr)
    {
        $info = new CheckinGroup();

        $info->grouptype = Utils::arrayGet($arr, "grouptype");
        $info->groupid = Utils::arrayGet($arr, "groupid");
        foreach ($arr["checkindate"] as $item) {
            $info->checkindate[] = CheckinDate::ParseFromArray($item);
        }
        foreach ($arr["spe_workdays"] as $item) {
            $info->spe_workdays[] = SpeWorkDays::ParseFromArray($item);
        }
        foreach ($arr["spe_offdays"] as $item) {
            $info->spe_offdays[] = SpeOffDays::ParseFromArray($item);
        }
        $info->sync_holidays = Utils::arrayGet($arr, "sync_holidays");
        $info->groupname = Utils::arrayGet($arr, "groupname");
        $info->need_photo = Utils::arrayGet($arr, "need_photo");
        foreach ($arr["wifimac_infos"] as $item) {
            $info->wifimac_infos[] = WifiMacInfo::ParseFromArray($item);
        }
        $info->note_can_use_local_pic = Utils::arrayGet($arr, "note_can_use_local_pic");
        $info->allow_checkin_offworkday = Utils::arrayGet($arr, "allow_checkin_offworkday");
        $info->allow_apply_offworkday = Utils::arrayGet($arr, "allow_apply_offworkday");
        foreach ($arr["loc_infos"] as $item) {
            $info->loc_infos[] = LocInfo::ParseFromArray($item);
        }

        return $info;
    }
}
