<?php

namespace wework\struct\checkin;

use wework\Utils;

class LocInfo
{
    /** @var int */
    public $lat = null;
    /** @var int */
    public $lng = null;
    /** @var string */
    public $loc_title = null;
    /** @var string */
    public $loc_detail = null;
    /** @var int */
    public $distance = null;

    public static function ParseFromArray($arr)
    {
        $info = new LocInfo();

        $info->lat = Utils::arrayGet($arr, "lat");
        $info->lng = Utils::arrayGet($arr, "lng");
        $info->loc_title = Utils::arrayGet($arr, "loc_title");
        $info->loc_detail = Utils::arrayGet($arr, "loc_detail");
        $info->distance = Utils::arrayGet($arr, "distance");

        return $info;
    }
}
