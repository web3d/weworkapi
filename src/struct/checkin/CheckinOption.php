<?php

namespace wework\struct\checkin;

use wework\Utils;

class CheckinOption
{
    /** @var CheckinInfo[]|array */
    public $info = null;

    static public function ParseFromArray($arr)
    {
        $info = new CheckinOption();

        foreach ($arr["info"] as $item) {
            $info->info[] = CheckinInfo::ParseFromArray($item);
        }

        return $info;
    }
}
