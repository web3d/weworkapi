<?php

namespace wework\struct;

use wework\struct\menu\MenuItem;
use wework\struct\menu\SubMenu;
use wework\struct\menu\ClickMenu;
use wework\struct\menu\ViewMenu;
use wework\struct\menu\ScanCodePushMenu;
use wework\struct\menu\ScanCodeWaitMsgMenu;
use wework\struct\menu\PicSysPhotoMenu;
use wework\struct\menu\PicPhotoOrAlbumMenu;
use wework\struct\menu\PicWeixinMenu;
use wework\struct\menu\LocationSelectMenu;

class Menu implements MenuItem
{
    /** @var MenuItem[]|array xxxMenu , 即各种子菜单array */
    public $button = null;

    public function __construct($xxmenuArray = null)
    {
        $this->button = $xxmenuArray;
    }

    public static function CheckMenuCreateArgs($menu)
    {

    }

    public static function Array2Menu($arr)
    {
        $menu = new Menu();

        foreach ($arr["button"] as $item) {
            $subButton = null;
            if (!array_key_exists("type", $item)) {
                $subButton = SubMenu::Array2Menu($item);
            } else {
                $type = $item["type"];
                if ($type == "click") {
                    $subButton = ClickMenu::Array2Menu($item);
                }
                if ($type == "view") {
                    $subButton = ViewMenu::Array2Menu($item);
                }
                if ($type == "scancode_push") {
                    $subButton = ScanCodePushMenu::Array2Menu($item);
                }
                if ($type == "scancode_waitmsg") {
                    $subButton = ScanCodeWaitMsgMenu::Array2Menu($item);
                }
                if ($type == "pic_sysphoto") {
                    $subButton = PicSysPhotoMenu::Array2Menu($item);
                }
                if ($type == "pic_photo_or_album") {
                    $subButton = PicPhotoOrAlbumMenu::Array2Menu($item);
                }
                if ($type == "pic_weixin") {
                    $subButton = PicWeixinMenu::Array2Menu($item);
                }
                if ($type == "location_select") {
                    $subButton = LocationSelectMenu::Array2Menu($item);
                }
            }
            $menu->button[] = $subButton;
        }

        return $menu;
    }

} // class
