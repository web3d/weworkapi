<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class PartyInfo
{
    /** @var int */
    public $id = null;
    /** @var bool */
    public $writable = null;

    static public function ParseFromArray($arr)
    {
        $info = new PartyInfo();

        $info->id = Utils::arrayGet($arr, "id");
        $info->writable = Utils::arrayGet($arr, "writable");

        return $info;
    }
}
