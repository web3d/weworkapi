<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class RegisterAuthUserInfo
{
    /** @var string */
    public $email = null;
    /** @var string */
    public $mobile = null;
    /** @var string */
    public $userid = null;

    static public function ParseFromArray($arr)
    {
        $info = new RegisterAuthUserInfo();

        $info->email = Utils::arrayGet($arr, "email");
        $info->mobile = Utils::arrayGet($arr, "mobile");
        $info->userid = Utils::arrayGet($arr, "userid");

        return $info;
    }
}
