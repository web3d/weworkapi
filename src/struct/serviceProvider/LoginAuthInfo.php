<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class LoginAuthInfo
{
    /** @var PartyInfo[]|array */
    public $department = null;

    static public function ParseFromArray($arr)
    {
        $info = new LoginAuthInfo();

        foreach ($arr["department"] as $item) {
            $info->department[] = PartyInfo::ParseFromArray($item);
        }
        return $info;
    }
}
