<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class LoginCorpInfo
{
    /** @var string */
    public $corpid = null;

    static public function ParseFromArray($arr)
    {
        $info = new LoginCorpInfo();

        $info->corpid = Utils::arrayGet($arr, "corpid");

        return $info;
    }
}
