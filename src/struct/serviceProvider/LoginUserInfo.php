<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class LoginUserInfo
{
    /** @var string */
    public $userid = null;
    /** @var string */
    public $name = null;
    /** @var string */
    public $avatar = null;
    /** @var string */
    public $email = null;

    static public function ParseFromArray($arr)
    {
        $info = new LoginUserInfo();

        $info->userid = Utils::arrayGet($arr, "userid");
        $info->name = Utils::arrayGet($arr, "name");
        $info->avatar = Utils::arrayGet($arr, "avatar");
        $info->email = Utils::arrayGet($arr, "email");

        return $info;
    }
}
