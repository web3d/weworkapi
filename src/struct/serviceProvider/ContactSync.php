<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class ContactSync
{
    /** @var string */
    public $access_token = null;
    /** @var int */
    public $expires_in = null;

    static public function ParseFromArray($arr)
    {
        $info = new ContactSync();

        $info->access_token = Utils::arrayGet($arr, "access_token");
        $info->expires_in = Utils::arrayGet($arr, "expires_in");

        return $info;
    }
}
