<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class SetAgentScopeReq
{
    /** @var int */
    public $agentid = null;
    /** @var string[]|array */
    public $allow_user = null;
    /** @var int[]|array */
    public $allow_party = null;
    /** @var int[]|array */
    public $allow_tag = null;

    public function FormatArgs()
    {
        $args = array();

        Utils::setIfNotNull($this->agentid, "agentid", $args);
        Utils::setIfNotNull($this->allow_user, "allow_user", $args);
        Utils::setIfNotNull($this->allow_party, "allow_party", $args);
        Utils::setIfNotNull($this->allow_tag, "allow_tag", $args);

        return $args;
    }
}

