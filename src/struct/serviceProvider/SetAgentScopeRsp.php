<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class SetAgentScopeRsp
{
    /** @var string[]|array */
    public $invaliduser = null;
    /** @var int[]|array */
    public $invalidparty = null;
    /** @var int[]|array */
    public $invalidtag = null;

    static public function ParseFromArray($arr)
    {
        $info = new SetAgentScopeRsp();

        $info->invaliduser = Utils::arrayGet($arr, "invaliduser");
        $info->invalidparty = Utils::arrayGet($arr, "invalidparty");
        $info->invalidtag = Utils::arrayGet($arr, "invalidtag");

        return $info;
    }
}
