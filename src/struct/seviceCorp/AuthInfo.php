<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class AuthInfo
{
    /** @var AgentBriefEx[]|array */
    public $agent = null;

    static public function ParseFromArray($arr)
    {
        $info = new AuthInfo();

        foreach ($arr["agent"] as $item) {
            $info->agent[] = AgentBriefEx::ParseFromArray($item);
        }

        return $info;
    }
}
