<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class AuthUserInfo
{
    /** @var string */
    public $email = null;
    /** @var string */
    public $mobile = null;
    /** @var string */
    public $userid = null;
    /** @var string */
    public $name = null;
    /** @var string */
    public $avatar = null;

    static public function ParseFromArray($arr)
    {
        $info = new AuthUserInfo();

        $info->email = Utils::arrayGet($arr, "email");
        $info->mobile = Utils::arrayGet($arr, "mobile");
        $info->userid = Utils::arrayGet($arr, "userid");
        $info->name = Utils::arrayGet($arr, "name");
        $info->avatar = Utils::arrayGet($arr, "avatar");

        return $info;
    }
}
