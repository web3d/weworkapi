<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class GetUserDetailBy3rdRsp
{
    /** @var string */
    public $corpid = null;
    /** @var string */
    public $userid = null;
    /** @var string */
    public $name = null;
    /** @var int[]|array */
    public $department = null;
    /** @var string */
    public $position = null;
    /** @var string */
    public $mobile = null;
    /** @var string */
    public $gender = null;
    /** @var string */
    public $email = null;
    /** @var string */
    public $avatar = null;

    static public function ParseFromArray($arr)
    {
        $info = new GetUserDetailBy3rdRsp();

        $info->corpid = Utils::arrayGet($arr, "corpid");
        $info->userid = Utils::arrayGet($arr, "userid");
        $info->name = Utils::arrayGet($arr, "name");
        $info->department = Utils::arrayGet($arr, "department");
        $info->position = Utils::arrayGet($arr, "position");
        $info->mobile = Utils::arrayGet($arr, "mobile");
        $info->gender = Utils::arrayGet($arr, "gender");
        $info->email = Utils::arrayGet($arr, "email");
        $info->avatar = Utils::arrayGet($arr, "avatar");

        return $info;
    }
}
