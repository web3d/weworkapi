<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class AgentBriefEx
{
    /** @var int */
    public $agentid = null;
    /** @var string */
    public $name = null;
    /** @var string */
    public $round_logo_url = null;
    /** @var string */
    public $square_logo_url = null;
    /** @var int */
    public $appid = null;
    /** @var AgentPrivilege */
    public $privilege = null;

    static public function ParseFromArray($arr)
    {
        $info = new AgentBriefEx();

        $info->agentid = Utils::arrayGet($arr, "agentid");
        $info->name = Utils::arrayGet($arr, "name");
        $info->round_logo_url = Utils::arrayGet($arr, "round_logo_url");
        $info->square_logo_url = Utils::arrayGet($arr, "square_logo_url");
        $info->appid = Utils::arrayGet($arr, "appid");

        if (array_key_exists("privilege", $arr)) {
            $info->privilege = AgentPrivilege::ParseFromArray($arr["privilege"]);
        }

        return $info;
    }
}
