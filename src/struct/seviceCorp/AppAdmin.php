<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class AppAdmin
{
    /** @var string */
    public $userid = null;
    /** @var int, 该管理员对应用的权限：0=发消息权限，1=管理权限 */
    public $auth_type = null;

    static public function ParseFromArray($arr)
    {
        $info = new AppAdmin();

        $info->userid = Utils::arrayGet($arr, "userid");
        $info->auth_type = Utils::arrayGet($arr, "auth_type");

        return $info;
    }
}
