<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class GetAuthInfoRsp
{
    /** @var AuthCorpInfo */
    public $auth_corp_info = null;
    /** @var AuthInfo */
    public $auth_info = null;

    static public function ParseFromArray($arr)
    {
        $info = new GetAuthInfoRsp();

        if (array_key_exists("auth_corp_info", $arr)) {
            $info->auth_corp_info = AuthCorpInfo::ParseFromArray($arr["auth_corp_info"]);
        }
        if (array_key_exists("auth_info", $arr)) {
            $info->auth_info = AuthInfo::ParseFromArray($arr["auth_info"]);
        }

        return $info;
    }
}
