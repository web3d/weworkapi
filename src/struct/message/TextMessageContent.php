<?php

namespace wework\struct\message;

use wework\exception\QyApiError;
use wework\Utils;

class TextMessageContent implements MessageContent
{
    public $msgtype = "text";
    /** @var string */
    private $content = null;

    public function __construct($content = null)
    {
        $this->content = $content;
    }

    public function CheckMessageSendArgs()
    {
        $len = strlen($this->content);
        if ($len == 0 || $len > 2048) {
            throw new QyApiError("invalid content length ".$len);
        }
    }

    public function MessageContent2Array(&$arr)
    {
        Utils::setIfNotNull($this->msgtype, "msgtype", $arr);

        $contentArr = array("content" => $this->content);
        Utils::setIfNotNull($contentArr, $this->msgtype, $arr);
    }
}
