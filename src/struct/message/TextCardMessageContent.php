<?php

namespace wework\struct\message;

use wework\Utils;

class TextCardMessageContent implements MessageContent
{
    public $msgtype = "textcard";
    /** @var string */
    public $title = null;
    /** @var string */
    public $description = null;
    /** @var string */
    public $url = null;
    /** @var string */
    public $btntxt = null;

    public function __construct($title = null, $description = null, $url = null, $btntxt = null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->url = $url;
        $this->btntxt = $btntxt;
    }

    public function CheckMessageSendArgs()
    {
        Utils::checkNotEmptyStr($this->title, "title");
        Utils::checkNotEmptyStr($this->description, "description");
        Utils::checkNotEmptyStr($this->url, "url");
    }

    public function MessageContent2Array(&$arr)
    {
        Utils::setIfNotNull($this->msgtype, "msgtype", $arr);

        $contentArr = array();
        {
            Utils::setIfNotNull($this->title, "title", $contentArr);
            Utils::setIfNotNull($this->description, "description", $contentArr);
            Utils::setIfNotNull($this->url, "url", $contentArr);
            Utils::setIfNotNull($this->btntxt, "btntxt", $contentArr);
        }
        Utils::setIfNotNull($contentArr, $this->msgtype, $arr);
    }
}
