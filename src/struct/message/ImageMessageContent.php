<?php

namespace wework\struct\message;

use wework\Utils;

class ImageMessageContent implements MessageContent
{
    public $msgtype = "image";
    /** @var string */
    private $media_id = null;

    public function __construct($media_id = null)
    {
        $this->media_id = $media_id;
    }

    public function CheckMessageSendArgs()
    {
        Utils::checkNotEmptyStr($this->media_id, "media_id");
    }

    public function MessageContent2Array(&$arr)
    {
        Utils::setIfNotNull($this->msgtype, "msgtype", $arr);

        $contentArr = array("media_id" => $this->media_id);
        Utils::setIfNotNull($contentArr, $this->msgtype, $arr);
    }
}
