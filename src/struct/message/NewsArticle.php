<?php

namespace wework\struct\message;

use wework\Utils;

class NewsArticle
{
    /** @var string */
    public $title = null;
    /** @var string */
    public $description = null;
    /** @var string */
    public $url = null;
    /** @var string */
    public $picurl = null;
    /** @var string */
    public $btntxt = null;

    public function __construct($title = null, $description = null, $url = null, $picurl = null, $btntxt = null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->url = $url;
        $this->picurl = $picurl;
        $this->btntxt = $btntxt;
    }

    public function CheckMessageSendArgs()
    {
        Utils::checkNotEmptyStr($this->title, "title");
        Utils::checkNotEmptyStr($this->url, "url");
    }

    public function Article2Array()
    {
        $args = array();

        Utils::setIfNotNull($this->title, "title", $args);
        Utils::setIfNotNull($this->description, "description", $args);
        Utils::setIfNotNull($this->url, "url", $args);
        Utils::setIfNotNull($this->picurl, "picurl", $args);
        Utils::setIfNotNull($this->btntxt, "btntxt", $args);

        return $args;
    }
}
