<?php

namespace wework\struct\batch;

use wework\Utils;

class BatchJobResult
{
    const STATUS_PENDING = 1;
    const STATUS_STARTED = 2;
    const STATUS_FINISHED = 3;

    /** @var int, 任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成 */
    public $status = null;
    /** @var string, 操作类型，目前分别有：sync_user(增量更新成员) replace_user(全量覆盖成员) replace_party(全量 覆盖部门) */
    public $type = null;
    /** @var int, 任务运行总条数 */
    public $total = null;
    /** @var int, 目前运行百分比，当任务完成时为100 */
    public $percentage = null;
    /** @var mixed 参考文档 */
    public $result = null;
} // BatchJobResult
