<?php

namespace wework\struct\batch;

use wework\Utils;

class CallBack
{
    public $url = null; // string
    public $token = null; // string
    public $encodingaeskey = null; // string
} // class CallBack
