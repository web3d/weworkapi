<?php

namespace wework\struct\user;

class ExtattrItem
{
    public $name = null;
    public $value = null;

    public function __construct($name = null, $value = null)
    {
        $this->name = $name;
        $this->value = $value;
    }
}
