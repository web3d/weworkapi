<?php

namespace wework\struct;

use wework\Utils;
use wework\struct\batch\BatchJobResult;

class Batch
{

    static public function CheckBatchJobArgs($batchJobArgs)
    {
        Utils::checkNotEmptyStr($batchJobArgs->media_id, "media_id");
    }

    static public function Array2BatchJobResult($arr)
    {
        $batchJobResult = new BatchJobResult();

        $batchJobResult->status = Utils::arrayGet($arr, "status");
        $batchJobResult->type = Utils::arrayGet($arr, "type");
        $batchJobResult->total = Utils::arrayGet($arr, "total");
        $batchJobResult->percentage = Utils::arrayGet($arr, "percentage");
        $batchJobResult->result = Utils::arrayGet($arr, "result");

        return $batchJobResult;
    }

    static public function IsJobFinished($batchJobResult)
    {
        return !is_null($batchJobResult->status) && $batchJobResult->status == BatchJobResult::STATUS_FINISHED;
    }
} // class Batch
