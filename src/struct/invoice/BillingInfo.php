<?php

namespace wework\struct\invoice;

use wework\Utils;

class BillingInfo
{
    /** @var string */
    public $name = null;
    /** @var string */
    public $num = null;
    /** @var string */
    public $unit = null;
    /** @var string */
    public $fee = null;
    /** @var string */
    public $price = null;
}
