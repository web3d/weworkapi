<?php

namespace wework\struct\invoice;

use wework\Utils;

class BatchGetInvoiceInfoReq
{
    /**
     * @var InvoiceItem[]|array
     */
    public $item_list = null;
}
