<?php

namespace wework\struct\invoice;

use wework\Utils;

class BatchUpdateInvoiceStatusReq
{
    /** @var string */
    public $openid = null;
    /** @var string */
    public $reimburse_status = null;

    /**
     * @var InvoiceItem[]|array
     */
    public $invoice_list = null;
}
