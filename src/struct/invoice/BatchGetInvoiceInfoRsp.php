<?php

namespace wework\struct\invoice;

use wework\Utils;

class BatchGetInvoiceInfoRsp
{
    /**
     * @var InvoiceInfo[]|array
     */
    public $item_list = null;
}
