<?php

namespace wework\struct\invoice;

use wework\Utils;

class InvoiceItem
{
    /** @var string */
    public $card_id = null;
    /** @var string */
    public $encrypt_code = null;
}
