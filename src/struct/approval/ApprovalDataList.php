<?php

namespace wework\struct\approval;

use wework\Utils;

class ApprovalDataList
{
    /** @var int */
    public $count = null;
    /** @var int */
    public $total = null;
    /** @var int */
    public $next_spnum = null;

    /**
     * @var ApprovalData[]|null
     */
    public $data = null;

    static public function ParseFromArray($arr)
    {
        $info = new ApprovalDataList();

        $info->count = Utils::arrayGet($arr, "count");
        $info->total = Utils::arrayGet($arr, "total");
        $info->next_spnum = Utils::arrayGet($arr, "next_spnum");
        foreach ($arr["data"] as $item) {
            $info->data[] = ApprovalData::ParseFromArray($item);
        }

        return $info;
    }
}
