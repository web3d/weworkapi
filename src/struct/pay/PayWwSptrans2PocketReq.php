<?php

namespace wework\struct\pay;

use wework\Utils;

class PayWwSptrans2PocketReq
{
    /** @var string */
    public $appid = null;
    /** @var string */
    public $mch_id = null;
    /** @var string */
    public $device_info = null;
    /** @var string */
    public $nonce_str = null;
    /** @var string */
    public $sign = null;
    /** @var string */
    public $partner_trade_no = null;
    /** @var string */
    public $openid = null;
    /** @var string */
    public $check_name = null;
    /** @var string */
    public $re_user_name = null;
    /** @var int */
    public $amount = null;
    /** @var string */
    public $desc = null;
    /** @var string */
    public $spbill_create_ip = null;
    /** @var string */
    public $workwx_sign = null;
    /** @var string */
    public $ww_msg_type = null;
    /** @var string */
    public $act_name = null;
}
