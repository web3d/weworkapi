<?php

namespace wework\struct\pay;

use wework\Utils;

class QueryWwSptrans2PocketReq
{
    /** string */
    public $nonce_str = null;
    /** string */
    public $sign = null;
    /** string */
    public $partner_trade_no = null;
    /** string */
    public $mch_id = null;
    /** string */
    public $appid = null;
}
