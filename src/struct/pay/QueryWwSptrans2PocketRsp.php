<?php

namespace wework\struct\pay;

use wework\Utils;

class QueryWwSptrans2PocketRsp
{
    /** @var string */
    public $return_code = null;
    /** @var string */
    public $return_msg = null;
    /** @var string */
    public $sign = null;
    /** @var string */
    public $result_code = null;
    /** @var string */
    public $partner_trade_no = null;
    /** @var string */
    public $mch_id = null;
    /** @var string */
    public $detail_id = null;
    /** @var string */
    public $status = null;
    /** @var string */
    public $reason = null;
    /** @var string */
    public $openid = null;
    public $transfer_name = null;
    public $payment_amount = null;
    public $transfer_time = null;
    public $desc = null;
}
