<?php

namespace wework\struct\pay;

use wework\Utils;

class SendWorkWxRedpackReq
{
    /** @var string */
    public $nonce_str = null;
    /** @var string */
    public $sign = null;
    /** @var string */
    public $mch_billno = null;
    /** @var string */
    public $mch_id = null;
    /** @var string */
    public $wxappid = null;
    /** @var string */
    public $sender_name = null;
    /** @var int */
    public $agentid = null;
    /** @var string */
    public $sender_header_media_id = null;
    /** @var string */
    public $re_openid = null;
    /** @var int */
    public $total_amount = null;
    /** @var string */
    public $wishing = null;
    /** @var string */
    public $act_name = null;
    /** @var string */
    public $remark = null;
    /** @var string */
    public $scene_id = null;
    /** @var string */
    public $workwx_sign = null;
}
