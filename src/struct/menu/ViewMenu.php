<?php

namespace wework\struct\menu;

use wework\Utils;

class ViewMenu implements MenuItem
{
    public $type = "view";
    /** @var string */
    public $name = null;
    /** @var string */
    public $url = null;

    public function __construct($name = null, $url = null)
    {
        $this->name = $name;
        $this->url = $url;
    }

    public static function Array2Menu($arr)
    {
        $menu = new ViewMenu();

        $menu->type = "view";
        $menu->name = Utils::arrayGet($arr, "name");
        $menu->url = Utils::arrayGet($arr, "url");

        return $menu;
    }
}
