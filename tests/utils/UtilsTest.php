<?php

namespace tests\wework\utils;

use PHPUnit\Framework\TestCase;
use wework\Utils;

class A
{
    public $a = "aaaa";
    public $b = 111;
}

class B
{
    public $c = "cccc";
    public $d = null; // A array
}


final class UtilsTest extends TestCase
{

    public function testObject2Array()
    {

        $a = new A();

        $b = new B();

        $b->b = array($a, $a);

        $result = Utils::Object2Array($b);

        $this->assertEquals(
            'cccc',
            $result['c']
        );

        $this->assertEquals(
            'aaaa',
            $result['b'][0]['a']
        );

        $this->assertEquals(
            'aaaa',
            $result['b'][1]['a']
        );
    }
}
