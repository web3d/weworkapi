<?php

namespace tests\wework\utils;

use wework\HttpUtils;
use PHPUnit\Framework\TestCase;


final class HttpUtilsTest extends TestCase
{
    public function testGet()
    {
         $output = HttpUtils::httpGet("http://localhost:8080");
         var_dump($output);
    }

    public function testPost()
    {
        $output = HttpUtils::httpPost("http://localhost:8080", "hello");
        var_dump($output);
    }
}
