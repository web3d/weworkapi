<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File DepartmentTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;
use wework\CorpAPI;
use wework\struct\Department;

final class DepartmentTest extends TestCase
{

    protected $departmentId;

    protected function setUp(): void
    {
        parent::setUp();

        $this->api = new CorpAPI($this->config['CORP_ID'], $this->config['CONTACT_SYNC_SECRET']);

        $this->departmentId = 9;
    }

    public function testDepartmentCreate()
    {
        $department = new Department();
        {
            $department->name = "department_1";
            $department->parentid = 1;
            $department->id = $this->departmentId;
        }
        $departmentId = $this->api->DepartmentCreate($department);
        echo $departmentId."\n";

    }

    public function testDepartmentUpdate()
    {
        $department = new Department();

        $department->id = 9;
        $department->name = "department_2";
        $this->api->DepartmentUpdate($department);
    }

    public function testDepartmentList()
    {
        $departmentList = $this->api->DepartmentList();
        var_dump($departmentList);
    }

    public function testDepartmentDelete()
    {
        $this->api->DepartmentDelete($this->departmentId);
    }
}
