<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File OauthTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;

final class OauthTest extends TestCase
{

    public function testGetUserInfoByCode()
    {
        $UserInfoByCode = $this->api->GetUserInfoByCode("IPzWnFmIQVf2wJFlJrln9-P-wqu7jeQsKyUKol1TWeU");
        var_dump($UserInfoByCode);

        $userDetailByUserTicket = $this->api->GetUserDetailByUserTicket($UserInfoByCode->user_ticket);
        var_dump($userDetailByUserTicket);

    }
}
