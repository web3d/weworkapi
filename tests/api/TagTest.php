<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File TagTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;
use wework\CorpAPI;
use wework\struct\Tag;
use Exception;

final class TagTest extends TestCase
{

    protected $departmentId;

    protected function setUp(): void
    {
        parent::setUp();

        $this->api = new CorpAPI($this->config['CORP_ID'], $this->config['CONTACT_SYNC_SECRET']);

        $this->departmentId = 9;
    }

    public function testTagCreate()
    {
        $tag = new Tag();
        {
            $tag->tagname = "tag_1";
        }
        $tagid = $this->api->TagCreate($tag);
        echo $tagid."\n";

        //
        $tag->tagid = $tagid;
        $tag->tagname = "tag_2";
        $this->api->TagUpdate($tag);

        //
        $invalidUserIdList = null;
        $invalidPartyIdList = null;
        $this->api->TagAddUser(
            $tagid,
            array("ZhuShengBen", "abelzhu", "aaaa", "bbbb"),
            array(1, 2, 2222, 3333),
            $invalidUserIdList,
            $invalidPartyIdList);
        var_dump($invalidUserIdList);
        var_dump($invalidPartyIdList);

        //
        $this->api->TagDeleteUser($tagid, null, array(1, 2, 222222), $invalidUserIdList, $invalidPartyIdList);

        //
        $tag = $this->api->TagGetUser($tagid);
        var_dump($tag);

        //
        $tagList = $this->api->TagGetList();
        var_dump($tagList);

        //
        $this->api->TagDelete($tagid);

    }
}

