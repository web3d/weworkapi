<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File AgentTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;
use Exception;
use wework\struct\Agent;

/**
 * 应用管理
 */
final class AgentTest extends TestCase
{

    public function testAgentSet()
    {
        $agent = new Agent();

        $agent->agentid = $this->config['APP_ID'];
        $agent->description = "I'm description";

        $this->api->AgentSet($agent);
    }

    public function testAgentGet()
    {
        $agent = $this->api->AgentGet($this->config['APP_ID']);
        var_dump($agent);
    }

    public function testAgentGetList()
    {
        $agentList = $this->api->AgentGetList();
        var_dump($agentList);
    }
}
