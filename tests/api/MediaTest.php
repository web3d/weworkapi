<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File MediaTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;

final class MediaTest extends TestCase
{

    private $mediaId;

    public function testMediaUpload()
    {
        $mediaId = $this->api->MediaUpload("TestConfig.php", "file");
        echo $mediaId."\n";

        $this->mediaId = $mediaId;
    }

    public function testMediaGet()
    {
        $data = $this->api->MediaGet($this->mediaId);
        var_dump($data);
    }
}
