<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File CheckinTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;
use wework\CorpAPI;

final class CheckinTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

        $this->api = new CorpAPI($this->config['CORP_ID'], $this->config['CHECKIN_APP_SECRET']);
    }

    public function testCheckinOptionGet()
    {
        $checkinOption = $this->api->CheckinOptionGet(1513760113, array("ZhuShengBen"));
        var_dump($checkinOption);
    }

    public function testCheckinDataGet()
    {
        $checkinDataList = $this->api->CheckinDataGet(1, 1513649733, 1513770113, array("ZhuShengBen"));
        var_dump($checkinDataList);
    }
}

