<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File PayTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;
use wework\struct\pay\SendWorkWxRedpackReq;

final class PayTest extends TestCase
{

    public function testSendWorkWxRedpack()
    {
        $SendWorkWxRedpackReq = new SendWorkWxRedpackReq();

        $SendWorkWxRedpackReq->nonce_str = "nonce_str";

        $this->api->SendWorkWxRedpack($SendWorkWxRedpackReq);
    }
}
