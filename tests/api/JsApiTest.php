<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File JsApiTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;

final class JsApiTest extends TestCase
{

    public function testTicketGet()
    {
        $ticket = $this->api->TicketGet();
        echo $ticket."\n";
    }

    public function testJsApiTicketGet()
    {
        $ticket = $this->api->JsApiTicketGet();
        echo $ticket."\n";
    }
}


