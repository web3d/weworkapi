#!/bin/bash

##
 # Copyright (C) 2017 All rights reserved.
 #
 # @File run.sh
 # @Brief
 # @Author abelzhu, abelzhu@tencent.com
 # @Version 1.0
 # @Date 2017-12-13
 #
 #

# ./vendor/bin/phpunit --bootstrap ./tests/bootstrap.php ./tests/utils/
#./vendor/bin/phpunit --bootstrap ./tests/bootstrap.php ./tests/api/AgentTest.php
./vendor/bin/phpunit --bootstrap ./tests/bootstrap.php ./tests/api/ApprovalTest.php
